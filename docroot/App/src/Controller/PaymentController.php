<?php

namespace App\Controller;

use App\Entity\Payment;
use App\Factory\HttpClientFactory;
use App\Form\PaymentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PaymentController extends AbstractController
{

    /**
     * @var HttpClientFactory
     */
    private $httpClientFactory;

    /**
     * TicketController constructor.
     */
    public function __construct(HttpClientFactory $httpClientFactory)
    {
        $this->httpClientFactory = $httpClientFactory;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        return $this->render('payment/index.html.twig');
    }

    /**
     * @Route("/payment", name="payment")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function paymentAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $payment = new Payment();
        $form = $this->createForm(PaymentType::class, $payment);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $lydiaClient = $this->httpClientFactory->create();
            $params = [
                "currency" => "EUR",
                "type" => "email",
                "vendor_token"=> $lydiaClient->getVendorToken()
            ];
            $payment = $form->getData();
            $params = array_merge($params, $payment->toArray());

            $response = $lydiaClient->doPayment($params);

            $payment->setRequestId($response->request_id)
                    ->setRequestUuid($response->request_uuid)
                    ->setMobileUrl($response->mobile_url);

            $em->persist($payment);
            $em->flush();

            $this->addFlash(
                'notice',
                $response->message
            );

            return $this->redirectToRoute('homepage');
        }

        return $this->render('payment/payment.html.twig', [
                'form' => $form->createView(),
        ]);
    }
}
