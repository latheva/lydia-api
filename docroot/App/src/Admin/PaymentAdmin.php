<?php

namespace App\Admin;

use App\Entity\Payment;
use App\Factory\HttpClientFactory;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

final class PaymentAdmin extends AbstractAdmin
{

    protected $baseRouteName = 'payment';

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['list', 'sync_action']);

    }


    protected function configureListFields(ListMapper $listMapper)
    {
        // Sync with Lydia should be handles separately
        // todo: better to move this sync function in to a custom action
        $em = $this->modelManager->getEntityManager(Payment::class);
        $repo = $em->getRepository(Payment::class);
        $payments = $repo->findAll();

        $httpClientFactory = $this->getConfigurationPool()->getContainer()->get('http.client.factory');
        $lydiaClient = $httpClientFactory->create();

        foreach ($payments as $payment) {
            $params = [
                'request_id' => $payment->getRequestId()
            ];
            $res = $lydiaClient->getStatus($params);
            $status = $lydiaClient->transStatus($res->state);

            $payment->setStatus($status);
            $em->persist($payment);
        }
        $em->flush();
        //

        $listMapper
            ->addIdentifier('firstName')
            ->add('lastName')
            ->add('recipient')
            ->add('amount')
            ->add('status')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('firstName')
            ->add('lastName')
        ;
    }

}