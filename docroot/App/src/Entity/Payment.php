<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Traits\TimestampTrait;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaymentRepository")
 */
class Payment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    private $recipient;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $requestId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $requestUuid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mobileUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    use TimestampTrait {
        TimestampTrait::__construct as private __timestampConstruct;
    }

    /**
     * Payment constructor.
     * @param $status
     */
    public function __construct()
    {
        $this->__timestampConstruct();
        $this->status = "Waiting..";
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Payment
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Payment
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRecipient(): ?string
    {
        return $this->recipient;
    }

    /**
     * @param string $recipient
     * @return Payment
     */
    public function setRecipient(string $recipient): self
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAmount(): ?string
    {
        return $this->amount;
    }

    /**
     * @param string $amount
     * @return Payment
     */
    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * @param mixed $requestId
     * @return Payment
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequestUuid()
    {
        return $this->requestUuid;
    }

    /**
     * @param mixed $requestUuid
     * @return Payment
     */
    public function setRequestUuid($requestUuid)
    {
        $this->requestUuid = $requestUuid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMobileUrl()
    {
        return $this->mobileUrl;
    }

    /**
     * @param mixed $mobileUrl
     * @return Payment
     */
    public function setMobileUrl($mobileUrl)
    {
        $this->mobileUrl = $mobileUrl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Payment
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'recipient' => $this->recipient,
            'amount' => $this->amount
        ];
    }
}
