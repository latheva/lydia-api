<?php

namespace App\Factory;


use App\Http\LydiaClient;

interface HttpClientFactoryInterface
{

    /**
     * @param array $configs
     * @return LydiaClient
     */
    public function create($configs = []);
}