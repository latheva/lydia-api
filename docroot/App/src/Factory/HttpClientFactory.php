<?php

namespace App\Factory;


use App\Http\LydiaClient;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class HttpClientFactory implements HttpClientFactoryInterface
{

    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * HttpClientFactory constructor.
     * @param ParameterBagInterface $params
     */
    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @param array $configs
     * @return LydiaClient|\App\Http\LydiaClient
     */
    public function create($configs = [])
    {
        $baseUri = $configs['base_uri']?? $this->params->get('lydia_base_uri');
        $vendorToken = $configs['vendor_token']?? $this->params->get('vendor_token');
        $lydiaClient = new LydiaClient($baseUri);
        $lydiaClient->setVendorToken($vendorToken);

        return $lydiaClient;
    }

}