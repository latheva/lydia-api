<?php
/**
 * Created by PhpStorm.
 * User: thearasa
 * Date: 2019-10-20
 * Time: 16:02
 */

namespace App\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;


class LydiaClient implements LydiaClientInterface
{
    const REQUEST_DO = '/api/request/do.json';
    const REQUEST_STATUS = '/api/request/state.json';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $baseUri;

    /**
     * @var string
     */
    private $vendorToken;

    /**
     * LydiaClient constructor.
     *
     * @param $baseUri
     * @param $vendorToken
     * @param int $timeout
     */
    public function __construct($baseUri, $timeout = 0)
    {
        $this->baseUri = $baseUri;
        $this->client = new Client(['timeout'  => $timeout]);
//        $this->vendorToken = $vendorToken;
    }

    /**
     * POST Request
     *
     * @param $endpoint
     * @param array $params
     * @param array $options
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post($endpoint, $params, $options = [])
    {
        $options = ['form_params' => $params] + $options;
        $response = $this->client->request('POST', $this->baseUri.$endpoint, $options);
        return $response;
    }

    /**
     * GET Request
     *
     * @param $endpoint
     * @param array $params
     * @param array $options
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get($endpoint, $params, $options = [])
    {
        $options = ['form_params' => $params] + $options;
        $response = $this->client->request('GET', $this->baseUri.$endpoint, $options);
        return $response;
    }

    /**
     * @param array $params
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function doPayment($params)
    {
        try {
            $response = $this->post(SELF::REQUEST_DO, $params);
            if($response && $response->getStatusCode() == 200) {
                $body = json_decode($response->getBody()->getContents());
                return $body;
            }
        } catch (ClientException $e) {
            echo Psr7\str($e->getRequest());
            echo Psr7\str($e->getResponse());
        }

    }

    /**
     * @param array $params
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getStatus(array $params)
    {
        try {
            $response = $this->post(SELF::REQUEST_STATUS, $params);
            if($response && $response->getStatusCode() == 200) {
                $body = json_decode($response->getBody()->getContents());
                return $body;
            }
        } catch (ClientException $e) {
            echo Psr7\str($e->getRequest());
            echo Psr7\str($e->getResponse());
        }

    }

    /**
     * @return string
     */
    public function getVendorToken(): string
    {
        return $this->vendorToken;
    }

    /**
     * @param string $vendorToken
     * @return LydiaClient
     */
    public function setVendorToken(string $vendorToken): LydiaClient
    {
        $this->vendorToken = $vendorToken;
        return $this;
    }

    /**
     * @param string $code
     * @return string
     */
    public function transStatus(string $code): string
    {
        $mapping = [
            "Waiting to be accepted"                                => '0',
            "request accepted"                                      => '1',
            "Refused by the payer"                                  => '5',
            "Cancelled by the owner"                                => '6',
            "Unknown (should be considered as an invalid request)"  => '-1'
        ];

        return array_search( $code, $mapping );
    }


}