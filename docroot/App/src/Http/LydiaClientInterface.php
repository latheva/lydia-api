<?php
/**
 * Created by PhpStorm.
 * User: thearasa
 * Date: 2019-10-20
 * Time: 15:59
 */

namespace App\Http;


interface LydiaClientInterface
{
    /**
     * @param $endpoint
     * @param array $params
     * @param array $options
     * @return mixed
     */
    public function post($endpoint, array $params, $options = []);

    /**
     * @param $endpoint
     * @param array $params
     * @param array $options
     * @return mixed
     */
    public function get($endpoint, array $params, $options = []);

}