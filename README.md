# Docker commands

### Create docker containers
get into the docker directory as working directory

`$ cd lydia-api/docker`

`$ docker-compose up -d --build`


### Access php server (docker container)
`$ bash -c "clear && docker exec -it LYDIA_PHP7_SERVEUR bash"`

#Assets (Encore/Webpack)

## Installing Encore in Symfony Applications
   Run these commands to install both the PHP and JavaScript dependencies in your project:
   
   `$ composer require symfony/webpack-encore-bundle`
   
   `$ yarn install`
   
   
   Ref: https://symfony.com/doc/current/frontend/encore/installation.html
   
## Install bootsrap, jquery

`$ yarn add bootstrap --dev`

`$ yarn add jquery popper.js --dev`
 

## Install sass-loader
`yarn add sass-loader@^7.0.1 node-sass --dev`

   
## Compile Assets (Encore/Webpack)
   
**compile assets once**
 `$ yarn encore dev`

**or, recompile assets automatically when files change**
 `$ yarn encore dev --watch`

**on deploy, create a production build**
 `$ yarn encore production`
 
Ref: https://symfony.com/doc/current/frontend/encore/simple-example.html

## Access site (on mac)

if your are updated your  /etc/hosts with 

127.0.0.1       app.local

then the localhost
https://app.local

you can still access the via https://localhost


## Access BO
(sonata user security is not been installed.)
https://localhost/back-admin

https://app.local/back-admin

## PhpMyAdmin
http://localhost:8081

http://app.local:8081

user: admin
pass: admin

